import java.util.Date;

import models.Customer;
import models.Visit;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("an");
        Customer customer2 = new Customer("minh","gold");
        System.out.println(customer1);
        System.out.println(customer2);
        Date date = new Date();
        Visit visit1 = new Visit("an",date,10000,50000);
        Visit visit2 = new Visit("minh",date,50000,200000);
        System.out.println(visit1);
        System.out.println(visit2);
    }
}
